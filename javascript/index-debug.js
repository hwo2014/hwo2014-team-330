'use strict';

var bot         = require('hwo/bot');
var darkSide    = require('hwo/behaviours/darkSide');

var serverHost  = 'testserver.helloworldopen.com';
var serverPort  = '8091';
var botName     = 'DarthCoder';
var botKey      = 'wIQFMxvmbKF6';

global.DEBUG = true;

// a bot embeded with the power of the dark side
var darthBot = Object.create( bot );
darthBot.init( botName, botKey );
darthBot.use( darkSide );

darthBot.onConnect = function(){
    //darthBot.join();
    darthBot.joinRace( 'germany', 1 );
};

darthBot.connect( serverPort, serverHost );

