'use strict';

var bot         = require('hwo/bot');
var darkSide    = require('hwo/behaviours/darkSide');

var serverHost  = process.argv[2];
var serverPort  = process.argv[3];
var botName     = process.argv[4];
var botKey      = process.argv[5];

global.DEBUG = true;

// a bot embeded with the power of the dark side
var darthBot = Object.create( bot );
darthBot.init( botName, botKey );
darthBot.use( darkSide );

darthBot.onConnect = function(){
    darthBot.join();
    //darthBot.joinRace( 'keimola', 1 );
    //darthBot.joinRace( 'germany', 1 );
    //darthBot.joinRace( 'usa', 1 );
};

darthBot.connect( serverPort, serverHost );

